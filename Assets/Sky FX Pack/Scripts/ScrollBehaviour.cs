using UnityEngine;
using System.Collections;

public class ScrollBehaviour : MonoBehaviour
{
    public int materialIndex = 0;
    public Vector2 uvAnimationRate = new Vector2(1.0f, 0.0f);
    public string textureName = "_MainTex";
    private Renderer ren;

    Vector2 uvOffset = Vector2.zero;

    private void Start()
    {
        ren = GetComponent<Renderer>();
    }

    void LateUpdate()
    {
        uvOffset += (uvAnimationRate * Time.deltaTime);
        if (ren.enabled)
        {
            ren.materials[materialIndex].SetTextureOffset(textureName, uvOffset);
        }
    }
}
