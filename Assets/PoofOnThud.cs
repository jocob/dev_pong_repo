﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoofOnThud : MonoBehaviour {

    private GameObject childParticle;
    private ParticleSystem pe;
    private float peDuration;
    private FlingAppendage fa;

    // Use this for initialization
    void Start () {
        fa = GetComponent<FlingAppendage>();

        childParticle = GetComponent<Transform>().Find("Poof Particle").gameObject;

        peDuration = 0.5f;
        if (childParticle != null)
        {
            pe = childParticle.GetComponent<ParticleSystem>();
            if (pe != null)
            {
                peDuration = pe.main.duration;
            }
        }
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (fa != null)
        {
            if (fa.Withering) return;
        }

        if (collision.gameObject.layer == 8)// Layer #8 is walls
        {
            if (childParticle != null)
            {
                childParticle.SetActive(true);
                Invoke("ReActivate", peDuration);
            }
        } 
    }

    private void ReActivate()
    {
        childParticle.SetActive(false);
    }


    // Update is called once per frame
    void Update () {
		
	}
}
