﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    This Script is a biological function of the core. It lowers gravity enough to halt falling
    when pressure is applied to wall layered objects.

    TODO: Currently the name of the object is used to determine if a wall can be stuck too. It
    would be more scalable if we based it on a layer or tag.

    TODO: It would be nice to show some sort of emotion on the player when this was occuring.

    TODO: More of a suggestion, but I think it would add more to gameplay if there was a limit
    to how long the player could stick to a wall before sliding down. Or, alternately, there
    could be a total time limit of wall sticking that started from the moment of jumping, so
    that there would be effective maximum heights that the player could reach.
*/

public class stickToWalls : MonoBehaviour {
    private float GravityScale;
    private bool isStuckWest = false;
    private bool isStuckEast = false;
    
    private GameObject LilDude;

    // Use this for initialization
    void Start () {
        LilDude = GameObject.Find("Lil Dude");
        GravityScale =LilDude.transform.GetComponent<Rigidbody2D>().gravityScale;
	}

    void OnCollisionStay2D(Collision2D col)
    {
       
        //Debug.Log(col.transform.name);
        if(col.transform.name=="West")
        {
            isStuckWest = true;
            //Debug.Log("Should Stickw");

        }
        if (col.transform.name == "East")
        {
            isStuckEast = true;
            //Debug.Log("Should Sticke");

        }

    }

    private void OnCollisionExit2D(Collision2D col)
    {
        if (col.transform.name == "West")
        {
            isStuckWest = false;
        }
        if (col.transform.name == "East")
        {
            isStuckEast = false;
        }
    }
    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyUp(KeyCode.LeftArrow))
            isStuckWest = false;
        if (Input.GetKeyUp(KeyCode.RightArrow))
            isStuckEast = false;

        if (isStuckWest || isStuckEast)
        {
            LilDude.transform.GetComponent<Rigidbody2D>().gravityScale = 0;

        }
        else
            LilDude.transform.GetComponent<Rigidbody2D>().gravityScale = GravityScale;


    }
}
