﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
    This Script is a biological function of limb members. It separates a limb from its parent and
    flings it toward the mouse input. Then, it will add force to the limb to bring it back and
    reattach it.

    TODO: This may be difficult, but I want this functionality to work automatically on new limbs.
    It would be great if a random compatible limb was fired. The limbs that are deemed compatible
    are those that have no children, are currently attached, and are not supporting the core's
    weight. (EDIT: I made some progress toward this goal. I have the appendages register with the
    Globals class, pick up an ID, and then only one ID is allowed to fling per frame)

    TODO: LimbStrength will determine the fling force, so it would make sense if the limb had a
    public variable that could be referenced by its "FlingAppendage" component. For now it's
    a public variable for testing.

    TODO: While I like that the compatible limbs can fire off automatically, it would be nice if
    the player had some way of toggling on control, or maybe the next limb to fire would glow or
    something to show it's ready. This way we could make limbs have different attributes and
    abilities and the player could work those attributes into their strategy. For example, limbs
    may be "bioluminescent" when thrown, and the player may want to fling that limb into darkness.
*/

public class FlingAppendage : MonoBehaviour {

    public float limbStrength;
    public int limbStamina;
    public float witherRate = 1.5f;
    public bool Withering { get; set; }

    private int flingID;
    private Transform thisTransform;
    private GameObject parentLimbOrCore;
    private Vector3 returnLocalPosition;
    private Vector3 returnLocalScale;
    private Quaternion returnLocalRotation;
    private bool attached;
    private int stamina;
    private float staminaSeconds;

	// Use this for initialization
	void Start () {
        flingID = Globals.GetFlingID();

        thisTransform = GetComponent<Transform>();

        parentLimbOrCore = thisTransform.parent.gameObject;
        returnLocalPosition = thisTransform.localPosition;
        returnLocalScale = thisTransform.localScale;
        returnLocalRotation = thisTransform.localRotation;

        Withering = false;

        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("P_layer"), LayerMask.NameToLayer("P_layer"));

        stamina = limbStamina > 0 ? limbStamina : 1;
        staminaSeconds = Mathf.Ceil(stamina / 30);
        staminaSeconds = staminaSeconds > witherRate ? witherRate : staminaSeconds;
    }
	
	// Update is called once per frame
	void Update () {
        Globals.thisFrame = Time.frameCount;

        attached = Attached();
        ReportToGlobal(attached);

        if (!attached) stamina = stamina < 0 ? limbStamina : stamina - 1;

        if (Input.GetMouseButtonDown(0))
        {
            Fling();
        }
        Return();
    }

    private Rigidbody2D ToggleRB(bool on=false)
    {
        Rigidbody2D rb2d = GetComponent<Rigidbody2D>();

        if (on)
        {
            if (GetComponent<Rigidbody2D>() == null) rb2d = gameObject.AddComponent<Rigidbody2D>();
            else rb2d = GetComponent<Rigidbody2D>();
            return rb2d;
        }
        else
        {
            if (GetComponent<Rigidbody2D>() != null) Destroy(rb2d);
            return null;
        }
    }

    private Rigidbody2D ToggleRB()
    {
        bool on = GetComponent<Rigidbody2D>() == null;
        return ToggleRB(!on);
    }

    private bool Attached()
    {
        if (parentLimbOrCore == null || thisTransform.parent == null) return false;

        if (thisTransform.parent.gameObject == parentLimbOrCore)
        {
            return true;
        }

        return false;
    }

    private void ReportToGlobal(bool report)
    {
        if (report)
        {
            if (!Globals.flingables.ContainsKey(flingID)) Globals.flingables.Add(flingID, 0);
            else Globals.flingables[flingID]++;
        }
        else
        {
            if (Globals.flingables.ContainsKey(flingID)) Globals.flingables.Remove(flingID);
        }
    }

    private void Detach()
    {
        ToggleRB(true);
        
        thisTransform.parent = null;
        thisTransform.localScale = returnLocalScale;
        thisTransform.localRotation = returnLocalRotation;
    }

    private void Grown()
    {
        Withering = false;
    }

    private void Reattach()
    {
        ToggleRB(false);

        thisTransform.SetParent(parentLimbOrCore.transform);
        thisTransform.localPosition = returnLocalPosition;
        thisTransform.localRotation = returnLocalRotation;
        thisTransform.DOScale(returnLocalScale, staminaSeconds);
        Invoke("Grown", staminaSeconds);
    }

    private void Wither()
    {
        Withering = true;
        thisTransform.DOScale(Vector3.zero, staminaSeconds);
        Invoke("Reattach", staminaSeconds);
    }

    private void Return()
    {
        if (attached) return;

        if (stamina <= 0)
        {
            Wither();
        }
    }

    private void Fling()
    {
        bool chosen = Globals.CanFling(flingID);

        if (!attached || !chosen)
        {
            Debug.Log(gameObject.ToString() + ": (FID: " + flingID.ToString() + ") (Attached:" + attached.ToString() + ") (Chosen: " + chosen.ToString() + ")");
            return;
        }

        Vector2 orig = parentLimbOrCore.transform.position;

        Detach();

        Vector2 target = GetMousePosition.With2D();

        target = target - orig;
        target = target.normalized;

        stamina = limbStamina;

        Debug.Log(gameObject.ToString() + ": " + flingID.ToString());
        ToggleRB(true).AddForce(target * limbStrength);
    }
}
