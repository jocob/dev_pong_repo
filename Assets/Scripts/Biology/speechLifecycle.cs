﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
    This Script is a biology function that handles the scheduled termination of speech generated
    by the player. It uses the life duration variable to decide how fast to scale and when to
    destroy the speech object.

    TODO: It would be great if speech objects did not terminate when a mouse was hovering over it.

    TODO: This may be a world function. It controls the behavior of what could be considered UI and
    it does so in a Game Controller sort of way. On that note, it may be something we can use for
    many other sorts of objects. Perhaps we could extend this to handle any object's life cycle
    calculations?
*/

public class speechLifecycle : MonoBehaviour {
    public float lifeDuration=3;
    private TextMesh theText;
    private Color myColor;
	// Use this for initialization
	void Start () {
        theText = transform.Find("text").GetComponent<TextMesh>();
        myColor = theText.color;
        transform.DOScale(transform.localScale.x * 2, lifeDuration);
        Invoke("destroyMe", lifeDuration);
	}
	
	private void destroyMe()
    {
        Destroy(this.gameObject);
    }
    private void Update()
    {
        myColor = new Color(myColor.r, myColor.g, myColor.b, Mathf.MoveTowards(myColor.a, 0, .02f/lifeDuration));//scale down alpha
        theText.color = myColor;
    }
}
