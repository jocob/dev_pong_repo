﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    This Script is a biological function of hand and foot members. It detects collisions with ground
    and wall layers, and will adjust the scale momentarily.

    TODO: Make this function automatically assigned to new feet and hands. The squishTime and
    squishAmount variables should be dynamically assigned based on the strength and weight of the
    appendage.

    TODO: <BUG> Now that I've got the "FlingAppendage" up and running, it has a biproduct of causing
    a glitch in the FeetSquish. Some appendages don't return to their original scale after being flung
    under an unknown combination of conditions.
    <FIX> It wasn't the scale, it was the rotation. When the player was twirling while shooting, the
    detached limb would loose its parentage and therefore not complete the twirl. I added a restore
    feature to FlingAppendage to fix it.
*/

public class FeetSquish : MonoBehaviour
{
    private Vector3 originalScale;
    public float squishTime = .025f;
    public float squishAmount = 2;

    // Adding a relationship to the FlingAppendage class
    private FlingAppendage fa;

    // Use this for initialization
    void Start()
    {
        originalScale = transform.localScale;

        // Adding the FlingAppendage component (if applicable)
        fa = GetComponent<FlingAppendage>();

        if (squishAmount == 0)
        {
            squishAmount = 1;
            //Debug.Log("Squish amount cannot be 0!");
        }
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        // Adding a nullifying statement to disable the squish while the limb is withering
        if (fa != null)
        {
            if (fa.Withering) return;
        }

        if (col.gameObject.layer == 8)// Layer #8 is walls
        {

            transform.DOScaleY(transform.localScale.y / squishAmount, squishTime);
            Invoke("resetScale", squishTime);
        }
    }
    private void resetScale()
    {
        transform.DOScaleY(originalScale.y, squishTime);

    }

    // Update is called once per frame
    void Update()
    {

    }
}
