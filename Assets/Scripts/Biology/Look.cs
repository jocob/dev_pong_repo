﻿using UnityEngine;
using System.Collections;

/*
    This Script is a biological function of eye members. It detects direction input and directs the
    pupils toward the direction of the input.

    TODO: Make this function automatically assigned to new eyes. The movement limits should be dynamic
    and based on the parent eyeball object's scale.

    TODO: Make the eye move independently toward the closest object that fits a certain criteria. One
    possible implementation is that there could be an array variable called "interesting" that contains
    object tags.

    TODO: Make the eyes emote. This may not fall under "look" activity, but it would be nice if the eyes
    behaved differently in different circumstances. The pupils should enlargen during happy times and
    shrink during danger. There could be a dizzy effect after too much jumping, boredom form inactivity,
    etc. It would also be cool to have wincing after collisions, and only the eyes affected by the
    collision should wince. So when the core collides, it winces, or if a limb with eyes walks, it can
    wince.
*/

public class Look : MonoBehaviour {

	public float glanceUpIntensity = 0f;
	public float glanceSideIntensity = 0f;

	private Transform tForm;
	private Vector3 originalPosition;

	// Use this for initialization
	void Start () {
		tForm = GetComponent<Transform> ();
		originalPosition = tForm.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		tForm.localPosition = originalPosition;
		if (Input.GetKey (KeyCode.UpArrow)) {
			tForm.localPosition = new Vector3 (
				tForm.localPosition.x, 
				tForm.localPosition.y + glanceUpIntensity, 
				tForm.localPosition.z);
		} else if (Input.GetKey (KeyCode.DownArrow)) {
			tForm.localPosition = new Vector3 (
				tForm.localPosition.x, 
				tForm.localPosition.y - glanceUpIntensity, 
				tForm.localPosition.z);
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			tForm.localPosition = new Vector3(
				tForm.localPosition.x + glanceSideIntensity, 
				tForm.localPosition.y, 
				tForm.localPosition.z);
		} else if (Input.GetKey (KeyCode.LeftArrow)) {
			tForm.localPosition = new Vector3(
				tForm.localPosition.x - glanceSideIntensity, 
				tForm.localPosition.y, 
				tForm.localPosition.z);
		}
	}
}
