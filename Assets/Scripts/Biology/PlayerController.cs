﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

/*
    This Script is a biology function for the main movement of the core member. It includes
    jumping, mid-air jumping, moving, and twirling.

    TODO: Jumping should become part of leg members' abilities.

    TODO: Double Jumping should be a part of the jumping script later, and it should have
    limitations that can be extended as the player grows.

    TODO: Twirling should also probably be its own script.

    TODO: It would be great if twirling were a skill acquired from either certain types of
    limbs or a certain level of strength in limbs.

    TODO: Twirling may be a good attacking skill, and we may even think about having it
    invoke the "FlingAppendage" as well.

    TODO: The player controller should grow to become the central manager for the player's
    biology. It should be aware of all of its limbs, be able to grow and atrophy, etc.
*/

public class PlayerController : MonoBehaviour
{

    public float slidePower = 1;
    public float bouncePower = 1;
    public int bounceCycleFrames = 1;
    public float rotateDuration = .25f;
    public float rotateDelay = .5f;
    private Transform tForm;

    private int currentBounceCycleFrame;
    private bool ableToBounce;

    // Use this for initialization
    void Start()
    {
        tForm = GetComponent<Transform>();
        currentBounceCycleFrame = 0;
        ableToBounce = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!ableToBounce)
        {
            if (currentBounceCycleFrame <= 0)
            {
                ableToBounce = true;
              //  Debug.Log("Bounce!");
            }
            else
            {
                currentBounceCycleFrame--;
            }
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (ableToBounce)
            {
                upMove();
                ableToBounce = false;
                currentBounceCycleFrame = bounceCycleFrames;
            }
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            leftMove();
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rightMove();
        }
    }

    public void leftMove()
    {
       

        transform.GetComponent<Rigidbody2D>().AddForce(Vector2.left * slidePower);
    }

    public void rightMove()
    {
        transform.GetComponent<Rigidbody2D>().AddForce(Vector2.right * slidePower);
    }

    public void upMove()
    {
        transform.GetComponent<Rigidbody2D>().AddForce(Vector2.up * bouncePower);
        Invoke("twirl", rotateDelay);
    }

    public void twirl()
    {
        //random parkour twirls
        int rand = Random.Range(1, 7);//1-6
        if (rand == 1)
            tForm.DOLocalRotate(new Vector3(0f, 0f, 360f), rotateDuration, RotateMode.LocalAxisAdd);
        else if (rand == 2)
            tForm.DOLocalRotate(new Vector3(0f, 0f, -360f), rotateDuration, RotateMode.LocalAxisAdd);
        else if (rand == 3)
            tForm.DOLocalRotate(new Vector3(0f, -360f, 0f), rotateDuration, RotateMode.LocalAxisAdd);
        else if (rand == 4)
            tForm.DOLocalRotate(new Vector3(0f, 360f, 0f), rotateDuration, RotateMode.LocalAxisAdd);
        else if (rand == 5)
            tForm.DOLocalRotate(new Vector3(-360f, 0f, 0f), rotateDuration, RotateMode.LocalAxisAdd);
        else
            tForm.DOLocalRotate(new Vector3(360f, 0f, 0f), rotateDuration, RotateMode.LocalAxisAdd);

    }
}


