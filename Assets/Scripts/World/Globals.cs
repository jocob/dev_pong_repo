﻿using System.Collections;
using System.Collections.Generic;
using System;

/*
    This Script is a world function built for storing variables that will be accessed from other
    classes. It is particularly helpful for not having to calculate the same value repeatedly.

    TODO: For now, I'm only using it to help manage limb flings, but it would be nice to look
    through other scripts and see if there is an oppurtunity to use this method for increasing
    efficiency elsewhere.

    TODO: I'm not sure if Globals could retain all the needed functionality if it inherited from
    monobehavior, so I didn't include it when I made the class. It would be great to test though,
    because then we wouldn't need to worry about passing in the "thisFrame" variable each frame
    and instead pull it straight from Time.frameCount.
*/

public class Globals {

    public static int thisFrame = 0;
    public static Dictionary<int, int> flingables = new Dictionary<int, int>();
    public static List<int> flingIDs = new List<int>();
    public static int chosenFID = 0;

    private static int setFrame = 0;
    private static Random random = new Random();

    public static int GetFlingID()
    {
        int fid = flingables.Count + 1;
        while (flingIDs.Contains(fid))
        {
            fid++;
        }

        flingIDs.Add(fid);
        return fid;
    }

    private static void ChooseForFrame()
    {
        if (setFrame != thisFrame)
        {
            if (flingables.Count > 0)
            {
                List<int> flingItems = new List<int>(flingables.Keys);
                chosenFID = flingItems[random.Next(flingItems.Count)];
            }
            
            setFrame = thisFrame;
        }
    }

    public static bool CanFling(int compare)
    {
        ChooseForFrame();
        if (chosenFID == compare) return true;
        return false;
    }
}
