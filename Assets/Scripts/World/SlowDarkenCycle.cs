﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*
    This Script is a world function intended to adjust the background overlay color to match night
    and day cycles.

    TODO: It would be great to include variables for weather and the mood of the scene. For instance,
    if the current event is a storm, it would be darker and grayer even if it's the middle of the day.
*/

public class SlowDarkenCycle : MonoBehaviour {

    public GameObject targetObject;
    public int cycles;
	public float speed;
	public Color second;
	public bool on = true;
	
	private Text text;
	private Image image;
	private RawImage rawImage;
	private SpriteRenderer spriteRenderer;
	private Color firstText;
	private Color firstImage;
	private Color firstRawImage;
	private Color firstSpriteRenderer;
	private int current;
	private bool toggle = true;
	
	// Use this for initialization
	void Start () {
		text = targetObject.GetComponent<Text>();
		image = targetObject.GetComponent<Image>();
		rawImage = targetObject.GetComponent<RawImage> ();
		spriteRenderer = targetObject.GetComponent<SpriteRenderer> ();
		current = cycles;
		if (text != null) {
			firstText = text.color;
		}
		if (image != null) {
			firstImage = image.color;
		}
		if (rawImage != null) {
			firstRawImage = rawImage.color;
		}
		if (spriteRenderer != null) {
			firstSpriteRenderer = spriteRenderer.color;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (toggle) {
			if(on){
				current--;
				if(text != null){
					text.color = Color.Lerp (text.color, second, speed);
				}
				if(image != null){
					image.color = Color.Lerp (image.color, second, speed);
				}
				if(rawImage != null){
					rawImage.color = Color.Lerp (rawImage.color, second, speed);
				}
				if(spriteRenderer != null){
					spriteRenderer.color = Color.Lerp (spriteRenderer.color, second, speed);
				}
				if (current <= 0) {
					current = 0;
					toggle = !toggle;
				}
			}
			else{
				toggle = false;
			}
		} else {
			current++;
			if(text != null){
				text.color = Color.Lerp (text.color, firstText, speed);
			}
			if(image != null){
				image.color = Color.Lerp (image.color, firstImage, speed);
			}
			if(rawImage != null){
				rawImage.color = Color.Lerp (rawImage.color, firstRawImage, speed);
			}
			if(spriteRenderer != null){
				spriteRenderer.color = Color.Lerp (spriteRenderer.color, firstSpriteRenderer, speed);
			}
			if (current >= cycles) {
				current = cycles;
				toggle = !toggle;
			}
		}
	}
	
	public void ActivateStrobe(){
		on = true;
	}
	
	public void DeactivateStrobe(){
		on = false;
	}
	
	public void ResetOriginalColor(Color newColor){
		if (text != null) {
			firstText = newColor;
		}
		if (image != null) {
			firstImage = newColor;
		}
		if (rawImage != null) {
			firstRawImage = newColor;
		}
		if (spriteRenderer != null) {
			firstSpriteRenderer = newColor;
		}
	}
}
