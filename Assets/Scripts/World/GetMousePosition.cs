﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    This Script is a world function with various methods that return the 3D position of the mouse
    as projected by the camera and settling on the z plane of the game object or transform you provide
    in the "With" mehod. If no argument is provided, then it will attempt to match the z plane of the
    first object with a "Player" tag, or use 0.0f if none can be found.

    TODO: I'd like it if the position could also be calculated with touch screen inputs instead.
*/

public class GetMousePosition : MonoBehaviour {

    public static float x = 0.0f;
    public static float y = 0.0f;
    public static float z = 0.0f;
    public static Vector3 xyz = Vector3.zero;

    private static Camera cam = Camera.main;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
    }

    private static void AssignValues()
    {
        xyz = cam.ScreenToWorldPoint(Input.mousePosition);
        x = xyz.x;
        y = xyz.y;
    }

    public static Vector3 With()
    {
        GameObject[] mainPlayer = GameObject.FindGameObjectsWithTag("Player");
        if (mainPlayer.Length > 0)
        {
            Transform tf = mainPlayer[0].GetComponent<Transform>();
            z = tf.position.z;
        }
        AssignValues();
        return new Vector3(x, y, z);
    }

    public static Vector3 With(Transform tf)
    {
        AssignValues();
        return new Vector3(x, y, tf.position.z);
    }

    public static Vector3 With(GameObject go)
    {
        AssignValues();
        Transform tf = go.GetComponent<Transform>();
        return new Vector3(x, y, tf.position.z);
    }

    public static Vector3 With2D()
    {
        AssignValues();
        return new Vector2(x, y);
    }
}
