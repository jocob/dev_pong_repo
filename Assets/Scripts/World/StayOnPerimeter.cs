﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
    This Script is a world function that positions walls around the border of the camera. It
    helps to restructure the openning level regardless of screen resolution or camera ratio.

    TODO: This script may not have any value long term. It's more for making sure there is a
    bounded perimeter for testing. However, some of the calculations may be helpful elsewhere,
    so it would be nice to use this script as a poster child for high abstraction of our
    various functions.
*/

public class StayOnPerimeter : MonoBehaviour {

	private Vector2 direction;
	private Transform tForm;
	private float constantZposition;
	private float constantZscale;
	private Camera mainCamera;
	private float cameraHeight;
	private Vector2 screenKeyDims;
	private Vector3 optimizedScreenCheck;

	// Use this for initialization
	void Start () {
		tForm = GetComponent<Transform> ();
		constantZposition = tForm.localPosition.z;
		constantZscale = tForm.localScale.z;
		direction = getDirectionFromName (tForm.name);

		mainCamera = Camera.main;
		changeTransformPositionAndScale ();

		optimizedScreenCheck = new Vector3 (Screen.width, Screen.height, mainCamera.orthographicSize);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 currentScreenCheck = new Vector3 (Screen.width, Screen.height, mainCamera.orthographicSize);

		if (Vector3.Distance (currentScreenCheck, optimizedScreenCheck) != 0) {
			changeTransformPositionAndScale();
		}

		optimizedScreenCheck = currentScreenCheck;
	}

	private Vector2 getDirectionFromName (string vector) {
		Dictionary<string, Vector2> directions = new Dictionary<string, Vector2> (4);
		directions.Add ("North", Vector2.up);
		directions.Add ("East", Vector2.right);
		directions.Add ("South", Vector2.down);
		directions.Add ("West", Vector2.left);

		if (directions.ContainsKey (vector)) {
			return directions [vector];
		} else {
			return new Vector2(1f, 1f);
		}
	}
	
	public Vector2 getScreenDimsFromCamera (float screenApexRadius) {
		float sW = (float)Screen.width;
		float sH = (float)Screen.height;
		float screenRatio = sW / sH;
		float screenHorizonRadius = screenApexRadius * screenRatio;
		return new Vector2 (screenHorizonRadius, screenApexRadius);
	}
	
	private float getXFromDirectionAndScreen (Vector2 dir, Vector2 dim) {
		return dim.x * dir.x;
	}
	
	private float getYFromDirectionAndScreen (Vector2 dir, Vector2 dim) {
		return dim.y * dir.y;
	}
	
	private float getWidthFromDirectionAndScreen (Vector2 dir, Vector2 dim) {
		return 1.0f + Mathf.Abs(dir.y) * (dim.x * 2.0f);
	}
	
	private float getHeightFromDirectionAndScreen (Vector2 dir, Vector2 dim) {
		return 1.0f + Mathf.Abs(dir.x) * (dim.y * 2.0f);
	}

	private void changeTransformPositionAndScale () {
		cameraHeight = mainCamera.orthographicSize;
		screenKeyDims = getScreenDimsFromCamera (cameraHeight);

		tForm.localPosition = new Vector3 (
			getXFromDirectionAndScreen (direction, screenKeyDims),
			getYFromDirectionAndScreen (direction, screenKeyDims),
			constantZposition);
		tForm.localScale = new Vector3 (
			getWidthFromDirectionAndScreen (direction, screenKeyDims),
			getHeightFromDirectionAndScreen (direction, screenKeyDims),
			constantZscale);
	}
}
